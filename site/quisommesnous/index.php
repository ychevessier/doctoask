<?php include('../header-footer/head.html'); ?>

<?php include('../header-footer/header.html'); ?>

<section class="contenu text-center">
    <!----Qui sommes-nous ?-->
    <div class="qsm-color">
        <div class="container">
            <h1 class="m-3">Qui sommes-nous&nbsp?</h1>
            <p class="mt-3">DoctoAsk est une application réalisée par des étudiants de l'Université de Bordeaux mettant en lien des médecins avec leurs patients via un chatbot. Notre équipe est composée de cinq étudiants, tous issus d'une formation technologique.
            </p>
        </div>
    </div>
    <!----Notre équipe-->
    <div class="container">
        <div class="qsm-border">
            <h1 class="qsm-title">Notre équipe</h1>
            <p>Nous sommes complémentaires, passionnés et spécialisés dans le développement et la programmation web. Nous ferons tout pour que votre recherche et votre expérience sur DoctoAsk se fasse dans les meilleures conditions !
            </p>
        </div>
        <div class="qsm-main">
            <div class="justify-content-center row">
                <!----Léo-Paul Dubourg-->
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <a href="https://www.linkedin.com/in/l%C3%A9o-paul-dubourg/" target="_blank"><img alt="Leo-Paul Dubourg" title="Léo-Paul Dubourg" src="../src/img/Leo-Paul.png"></a>
                    <h2>Léo-Paul Dubourg</h2>
                    <p>Chef de projet / Community Manager</p>
                </div>
                <!----Tom Quemener-->
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <a href="https://www.linkedin.com/in/tom-quemener/" target="_blank"><img class="img_hover2" alt="Tom Quemener" title="Tom Quemener" src="../src/img/Tom.png"></a>
                    <h2>Tom Quemener</h2>
                    <p>Développeur Back-end</p>
                </div>
                <!----Yoann Chevessier-->
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <a href="https://www.linkedin.com/in/yoannchevessier/" target="_blank"><img alt="Yoann Chevessier" title="Yoann Chevessier" src="../src/img/Yoann.png"></a>
                    <h2>Yoann Chevessier</h2>
                    <p>Coordinateur / Développeur Full-Stack</p>
                </div>
                <!----Jean Magné-->
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <a href="https://www.linkedin.com/in/jean-magne-18a63b153/" target="_blank"><img class="img_hover2" alt="Jean Magne" title="Jean Magné" src="../src/img/Jean.png"></a>
                    <h2>Jean Magné</h2>
                    <p>Développeur Full-Stack</p>
                </div>
                <!----Sébastien Leduc-->
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <a href="https://www.linkedin.com/in/sebastien-led/" target="_blank"><img alt="Sebastien Leduc" title="Sébastien Leduc" src="../src/img/Sebastien.png"></a>
                    <h2>Sébastien Leduc</h2>
                    <p>Graphiste / Intégrateur</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('../header-footer/footer.html'); ?>