<?php include('../header-footer/head.html'); ?>

<?php include('../header-footer/header.html'); ?>

    <!-- Contenu de la page -->

    <section class="container contenu">

        <!--  1 : La Presse en parle -->

        <div class="row">
            <h3 class="col-12 text-center p-4" style="color : purple ">

                <b> LA PRESSE EN PARLE</b>

            </h3>
        </div>
        <div class="row">
            <div class="col-lg-6 ">
                <div class="bg-presse center p-1">
                    <img src="img/sudouest.png" alt="sudouest" class="presse">
                </div>
                <p class="text-center p-2 ">DoctoAsk est la plateforme qui rapproche les médecins des patients dans les
                    desserts médicaux</p>
                <p class="text-right"><b>24/10/2018</b></p>
            </div>
            <div class="col-lg-6 ">
                
                    <div class="bg-presse center p-1">
                        <img src="img/LeMonde.png" alt="LeMonde" class="presse">
                    </div>
                
                <p class="text-center p-2">Des étudiants de Bordeaux ont créé un chatbot révolutionnaire.</p>
                <p class="text-right"><b>13/11/2018</b></p>
            </div>
        </div>

        <!-- 2 : DoctoAsk en quelques mots -->

        <div class="container ">
            <h3 class="text-center p-4" style="color : purple">
                <b>DOCTOASK EN QUELQUES MOTS </b>
            </h3>
            <p class="text-center">
                DoctoAsk permet aux médecins de fournir des réponses simples aux questions les plus courantes de
                leurs patients,
                tout en restant concentrés sur les tâches les plus importantes de leur travail quotidien.
                Plus besoin d'avoir à chercher sur internet ou d'appeler son cabinet pour obtenir des informations
                sur votre médecin,
                grâce à DoctoAsk, vous optiendrez rapidement vos réponses !
            </p>
        </div>

        <!-- 3 : Ressources -->
        <div class="row">
            <h3 class="col-12 text-center p-4" style="color : purple ">

                <b> RESSOURCES</b>

            </h3>
        </div>
        <div class="row ">
            <div class="col-lg-6 ">
                <div class="folder justify-content-center align-items-center mx-auto">
                    <span>Logos</span>
                </div>
                <div class="  text-center">
                    <button class="bg-sub ">Télécharger</button>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="folder justify-content-center align-items-center mx-auto">
                    <span>Images</span>
                </div>
                <div class="text-center">
                    <button class="bg-sub ">Télécharger</button>
                </div>
            </div>

        </div>




        <!-- 4 Communiqués de presse  -->

        <div class="container">
            <h3 class="text-center p-4" style="color : purple">
                <b>COMMUNIQUE DE PRESSE</b>
            </h3>
            <h6 class="text-center ">
                <p>Retrouvez nos communiqués officiels</p>
            </h6>
            <div>
                <p class="text-center p-4">
                    <b>1 Janvier 2019 : </b>Plus de 500 médecins fond déja confiance à DoctoAsk.
                </p>
            </div>
            <div class="col-12 p-4 text-center">
                <button class="bg-sub  p-2">
                    <h4><b>Nous contacter</b></h4>
                </button>

            </div>

        </div>


    </section>
    <!-- FOOTER -->

<?php include('../header-footer/footer.html'); ?>
