<!DOCTYPE html>
<html>

<head lang="fr" dir="ltr">
    <meta charset="utf-8">
    <title>DoctoAsk</title>
    <link rel="icon" type="image/png" href="img/favicon.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- meta facebook -->
    <meta property="og:title" content="Doctoask.com, Inscrivez-vous à la newsletter" />
    <meta property="og:url" content="https://doctoask.com" />
    <meta property="og:site_name" content="DoctoAsk" />
    <meta property="og:image" content="https://doctoask.com/img/social/carFb.jpg" />


    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://doctoask.com/doctostyle/doctostyle.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127739125-1"></script>

</head>

<body>
    <header>
        <!-- Header  -->
    </header>
    <section class="contenu">
        <!-- Contenu de la page -->

        <div class="container ">

            <h2 class="text-center col-12 p-4" style="color : purple">
                <b>Mot de passe oublié ? </b>
            </h2>

            <div class="mx-auto p-4 profil-img mx-auto justify-content-center align-items-center col-12">



                <div class="justify-content-center align-items-center">
                    <img src="../src/img/logo.png" alt="logo doctoask">
                </div>

            </div>
            <form action="" method="POST">
                <div class="justify-content-center">
                    <div class="col-lg-10 mx-auto">
                        <div class="row">
                            <div class="col-12 p-4">
                                <!----Email-->
                                <label class="col-12 text-center" style="font-size: 1.5em;">Entrer votre identifiant :</label>
                                <input type="email" name="email" placeholder="Email" required autofocus class="inClass col-12">

                            </div>
                            <div class="col-12 p-4">
                                <!----Récupérationmot de passe-->
                                <input type="submit " class="bg-sub col-12 text-center" value="Récupération de mot de passe">
                            </div>

                        </div>
    </section>

    </section>
    <!-- FOOTER -->

    <footer>

    </footer>
</body>

</html>