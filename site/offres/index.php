<?php include('../header-footer/head.html'); ?>

<?php include('../header-footer/header.html'); ?>

    <!-- Contenu de la page -->

    <!-- 1 : Description des offres -->
    <section class="contenu">
    <div class="offres">
        <div class="container">
            <h1 class="text-center pt-4" >
                Nos offres
            </h1>
            <p class="text-center p-4">
                DoctoAsk vous propose 2 tarifs différents, chacun contenant un lot de questions/réponses à configurer pour le chatbot, le nombre dépendant du tarif choisi. Il s’agit d’un abonnement mensuel, la première année étant gratuite afin de vous faire découvrir notre service. 
            </p>
        </div>
    </div>

    <!-- 2 : Les Offres -->
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-6 p-3">
                <div class="col-12">
                    <div class="img-offres align-items-end mx-auto" style="background-image : url(img/ordinateur.gif);">
                        <div class="text-center p-2">
                            <p><b>70 € par mois</b><br>25 réponses + première année offerte</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 p-3">
                <div class="col-12">
                    <div class="img-offres align-items-end mx-auto " style="background-image : url(img/tablette.jpg);">
                        <div class="text-center p-2">
                            <p><b>105 € par mois</b><br>50 réponses + première année offerte</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="container">
        <p class="text-center font-italic p-4">
            Une fois l’offre sélectionnée, et votre inscription terminée, vous pourrez alors configurer, dans votre espace personnel, vos questions/réponses, ainsi que tester le chatbot tel que le visiteur le percevra.
        </p>
    </div>
    <div class="col-12 p-4 pb-5 text-center">
       <a href="../besoindaide/index.php"> <button class="bg-sub p-2">
            <p>Nous contacter</p>
        </button></a>

    </div>

    </section>

    <!-- FOOTER -->

<?php include('../header-footer/footer.html'); ?>
