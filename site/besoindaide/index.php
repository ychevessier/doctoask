<?php include('../header-footer/head.html'); ?>

<?php include('../header-footer/header.html'); ?>

<section class="contenu text-center">
    <!--Barre de recherche-->
    <div class="ba-search">
        <h1>Centre d'aide</h1>
        <div class="search-bar">
            <a href="#"><i class="search-icon fa fa-search"></i></a>
            <input type="text" id="search" name="search" size="40" placeholder="Rechercher">
        </div>
    </div>
    <!--Problèmes-->
    <div class="container">
        <div class="problems justify-content-center row">
            <a href="#" title="Nouveauté"><div>Nouveauté</div></a>
            <a href="#" title="Chatbot"><div>Chatbot</div></a>
            <a href="#" title="Sécurité"><div>Sécurité</div></a>
            <a href="#" title="FAQ"><div>FAQ</div></a>
        </div>
    </div>
</section>

<?php include('../header-footer/footer.html'); ?>
