<?php include('../header-footer/head.html'); ?>

<?php include('../header-footer/header.html'); ?>

    <section class="contenu">
      <div class="container-fluid py-4">
        <div class="row">
          <div class="mx-auto">
            <img src="../src/img/pp-medecin/dr_bertrand.jpg" alt="photo de profil Dr Bertrand">
          </div>
        </div>
        <div class="row pt-4">
          <h1 class="mx-auto">Dr. Bertrand</h1>
        </div>
        <div class="row">
          <h2 class="mx-auto">Médecin neurologue</h2>
        </div>
      </div>
      <div class="container pt-3 pb-5">
        <div class="row justify-content-center">
          <div class="jumbotron col-12 col-md-5 m-1">
            <h1>Présentation</h1>
            <p>Le docteur Bertrand vous accueille dans son cabinet à Bordeaux, spécialisé en neurologie il saura vous conseiller pour toutes questions liées à son domaine... </p>
          </div>
          <div class="jumbotron col-12 col-md-5 m-1">
            <h1>Horaires</h1>
            <p>Votre médecin est ouvert du lundi au vendredi de 9h à 20h et le samedi de 10h à 14h</p>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="jumbotron col-12 col-md-5 m-1">
            <h1>Tarifs et remboursement</h1>
            <p>Consultation : 25€</p>
            <p>Convention secteur 1</p>
            <p>Carte vitale acceptée</p>
          </div>
          <div class="jumbotron col-12 col-md-5 m-1">
            <h1>Moyen de paiement</h1>
            <p><span class="badge badge-success">Carte bleu</span></p>
            <p><span class="badge badge-success">Chèque</span></p>
            <p><span class="badge badge-success">Espèce</span></p>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="jumbotron col-12 col-md-5 m-1">
            <h1>Site web de votre médecin</h1>
            <a class="btn btn-primary" href="#" role="button"><i class="fas fa-link"></i> Voir le site</a>
          </div>
          <div class="jumbotron col-12 col-md-5 m-1">
            <h1>Profil Doctolib</h1>
            <a class="btn btn-primary" href="#" role="button"><i class="fas fa-user-md"></i> Voir la page</a>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="jumbotron col-12 col-md-10 m-1">
            <h1>Localisation</h1>
            <p>38 rue de Pessac <br> 33000 Bordeaux</p>
            <div style="overflow:hidden;width: 100%;height: 440px;" class="mx-auto">
              <iframe  width="100%" height="100%" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=en&amp;q=38%20route%20de%20Pessac%20Bordeaux+(Titre)&amp;ie=UTF8&amp;t=&amp;z=13&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
          </div>
        </div>
      </div>
        <div class="col-12 col-md-4 btn-chat fixed-bottom mx-auto">
          <a href="#"><i class="fas fa-comment-dots"></i> Parler avec votre medecin</a>
        </div>
    </section>

<?php include('../header-footer/footer.html'); ?>
